/*
 * Professionnel est quelque'un qui donne des services
 * chez #GYM 
 */

public class Professionnel {
	int id;
	String nom;
	String email;
	String services;

	public Professionnel(int id, String nom, String email, String services) {
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.services = services;
		
	}
}

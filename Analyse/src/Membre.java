/*
 * Classe membre est quelqu'un qui a acces a #GYM
 */
public class Membre {
	int id;
	String nom;
	String email;
	String paye;
	
	public Membre(int id, String nom, String email, String paye) {
		this.id = id;
		this.nom = nom;
		this.email = email;
		this.paye = paye;
	}
}

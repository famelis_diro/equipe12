/*
 * Classe service, peut etre tout activite offerte par un professionnel #GYM
 * 
 *  voir doc sur https://docs.oracle.com/javase/8/docs/api/java/time/package-summary.html
 *  Facile a ce servir des types dates
 */

import java.time.*;


public class Service {
	int id;
	String nom;
	LocalDateTime dateHeureActuelles;
	LocalDate dateDebService;
	LocalDate dateFinService;
	LocalTime heureService;
	String[] recurrenceService; 
	int capaciteMaximale;
	int profId; 	
	double frais;
	String commentaires = "";
	
	//Utilsee dans le controlleur pour nouveau service
	public Service(		int id,
						String nom,
						LocalDate dateDebService, 
						LocalDate dateFinService, 
						LocalTime heureService,
						String[] recurrenceService, 
						int capaciteMaximale,
						int profId,
						double frais,
						String commentaires) {
		this.id = id;
		this.nom = nom;
		this.dateHeureActuelles = java.time.LocalDateTime.now();
		this.dateDebService = dateDebService;
		this.dateFinService = dateFinService;
		this.heureService = heureService;
		this.recurrenceService = recurrenceService;
		this.capaciteMaximale = capaciteMaximale;
		this.profId = profId;
		this.frais = frais;
		this.commentaires = commentaires;
		//this.CodeService = CentreServiceController.generateCodeService(); 
		
	}
	
	// utilsee pour recuperer des services qui existent deja
	public Service(	int id,
					String nom, 
					LocalDateTime dateHeureCreer, 
					LocalDate dateDebut, 
					LocalDate dateFin,
					LocalTime heure, 
					String[] recurrences, 
					int capaciteMax, 
					int profId,
					double frais,
					String commentaires) {
		this.id = id;
		this.nom = nom;
		this.dateHeureActuelles = dateHeureCreer;
		this.dateDebService = dateDebut;
		this.dateFinService = dateFin;
		this.heureService = heure;
		this.recurrenceService = recurrences;
		this.capaciteMaximale = capaciteMax;
		this.profId = profId;
		this.frais = frais;
		this.commentaires = commentaires;
		
	}
	

}

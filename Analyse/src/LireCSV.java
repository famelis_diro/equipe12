
import java.io.File;
import java.io.FileNotFoundException;
import java.time.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LireCSV {
	static Scanner scanner;
    
	// Cette methode mets tous les membres dans un dictionnaires
	public static Map<Integer,Membre> hashMembre(){
		ouvrirFichier("membre.csv");
		Map<Integer, Membre> map = new HashMap<Integer, Membre>();
		enleverEntete(4);
        while(scanner.hasNext()){
    		int id = scanner.nextInt();
    		String name = scanner.next();
    		String email = scanner.next();
    		String paye = scanner.next();
    		map.put(id, new Membre(id, name, email, paye));	
        }
        scanner.close();
		return map;
	}
	// Cette methode met tous les presences dans un dictionnaire
	public static Map<Integer,Presence> hashPresence(){
		ouvrirFichier("presence.csv");
		Map<Integer, Presence> map = new HashMap<Integer, Presence>();
		enleverEntete(6);
        while(scanner.hasNext()){
    		int id = scanner.nextInt();
    		String dateCreeStr = scanner.next();
    		LocalDateTime dateCree = LocalDateTime.parse(dateCreeStr);
    		int profId = scanner.nextInt();
    		int memId = scanner.nextInt();
    		int serviceId = scanner.nextInt();
    		String commentaires = scanner.next();
    		map.put(id, new Presence(id, dateCree, profId, memId, serviceId, commentaires));	
        }
        scanner.close();
		return map;
	}
	
	// Cette methode met tous les seances dans un dictionnaire
	public static Map<Integer,Seance> hashSeance(){
		ouvrirFichier("seance.csv");
		Map<Integer, Seance> map = new HashMap<Integer, Seance>();
		enleverEntete(7);
		while(scanner.hasNext()) {
			int id = scanner.nextInt();
			String dateHeureCreationStr = scanner.next();
			LocalDateTime dateHeureCreation = LocalDateTime.parse(dateHeureCreationStr);
			String dateServiceStr = scanner.next();
			LocalDate dateService = LocalDate.parse(dateServiceStr);
			int memId = scanner.nextInt();
			int profId = scanner.nextInt();
			int serviceId = scanner.nextInt();
			String commentaires = scanner.next();
			map.put(id, new Seance(id, dateHeureCreation, dateService, memId, profId, serviceId, commentaires));
		}
		scanner.close();
		return map;
	}
	
	// Cette methode met tous les service dans un dictionnaire
	public static Map<Integer,Service> hashService(){
		ouvrirFichier("services.csv");
		Map<Integer,Service> map = new HashMap<Integer,Service>();
		enleverEntete(11);
		while(scanner.hasNext()) {
			int id = scanner.nextInt();
			String name = scanner.next();
			String dateHeureCreerStr = scanner.next();
			LocalDateTime dateHeureCreer = LocalDateTime.parse(dateHeureCreerStr);
			String dateDebutStr = scanner.next();
			LocalDate dateDebut = LocalDate.parse(dateDebutStr);
			String dateFinStr = scanner.next();
			LocalDate dateFin = LocalDate.parse(dateFinStr);
			String heureStr = scanner.next();
			LocalTime heure = LocalTime.parse(heureStr);
			String recurrencesStr = scanner.next();
			String[] recurrences = recurrencesStr.split("  ");
			int capaciteMax = scanner.nextInt();
			int profId = scanner.nextInt();
			double frais = Double.parseDouble(scanner.next());
			String commentaires = scanner.next();
			
			map.put(id, new Service(id, name, dateHeureCreer, dateDebut, dateFin, heure, recurrences, capaciteMax, profId, frais, commentaires));
		}
		scanner.close();
		return map;
	}
	// Cette methode retourne un dictionnaire avec tous les professionnels dans le systeme
	public static Map<Integer, Professionnel> hashProfessionnel() {
		ouvrirFichier("professionnel.csv");
		Map<Integer, Professionnel> map = new HashMap<Integer, Professionnel>();
		enleverEntete(4);
        while(scanner.hasNext()){
    		int id = scanner.nextInt();
    		String name = scanner.next();
    		String email = scanner.next();
    		String services = scanner.next();
    		map.put(id, new Professionnel(id, name, email, services));	
        }
        scanner.close();
		return map;
	}
	
	// Cette methode enleve les entetes de titre dans le fichier csv
    public static void enleverEntete(int fois) {
    	for(int i = 0; i < fois; i++) {
    		scanner.next();
    	}
    }
    public static void ouvrirFichier(String fileName) {
    	try{
    		scanner = new Scanner(new File(fileName));
    	} catch(FileNotFoundException e) {
    		
    	}
    	scanner.useDelimiter(",|\n");
    }
    public static void main(String[] args) {
        
    }

	public static String lireRepertoireServices() {
		String temp="";
		ouvrirFichier("services.csv");
		enleverEntete(11);
		 while(scanner.hasNext()){
	    		temp += scanner.next() +" "; // service Id Code
	    		temp += scanner.next() +" "; // nom du service
	    		scanner.next(); // Date cr�ation skip
	    		scanner.next(); // Date D�but skip
	    		scanner.next(); // Date Fin skip
	    		temp += " Heure: "+scanner.next()+" "; // Heure du service
	    		temp += " Jours: "+scanner.next()+" "; // Recurrence
	    		temp += " Capacit� Max. : "+scanner.next()+" ";//Capacit� max
	    		scanner.next(); // Professionnel qui donne le service skip
	    		temp += " Frais : "+scanner.next()+"$ "; // Frais
	    		scanner.next();//skip commentaires
	    		temp += "\r\n";
	        }
		return temp;
	}
	
	public static String lireRepertoireInscriptions (int serviceId) {
		String temp="";
		String membreId = "";
		ouvrirFichier("seance.csv");
		enleverEntete(11);
		
		 while(scanner.hasNext()){
	    		scanner.next(); // skip int id
	    		scanner.next(); // skip dateHeureCreation
	    		scanner.next(); // skip Date service
	    		membreId += scanner.next(); // memId
	    		scanner.next(); // skip profId
	    		
	    		if(Integer.parseInt(scanner.next()) == serviceId) {
	    			temp += membreId;
	    		}
	    		scanner.next();//skip commentaires
	    		temp += "\r\n";
	        }
		return temp;
	}
	
	// Cette methode sert dans le cas de supression
	public static String nouveauRepertoireServicesSansUneEntree(int id){
		try {
			ouvrirFichier("services.csv");
		}
		catch (Exception e) {
			System.out.println("Impossible d'ouvrir/de lire le fichier services.csv");
			return null;
		}

		String temp= "";
		String ident;
		boolean found =false;
		int identInt=0;
		while(scanner.hasNext()){
			ident = scanner.next();
			try {identInt =Integer.parseInt(ident);}
			catch (Exception e) {}
			if (ident == "id" || identInt != id) {
				temp+= ident;
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+='\n';
			}
			else {
				scanner.next();// Pour skipper les champs de l'entree qu'on veut effacer
				scanner.next();
				scanner.next();
				scanner.next();
				scanner.next();
				scanner.next();
				scanner.next();
				found = true;
			}
		}
		scanner.close();
		if (found) {return temp;}
		else {
			System.out.println("Erreur: Pas de service avec ce code dans le systeme");
			return null;}
		
	}

	public static String nouveauRepertoireMembre(int membreId, String champ, String modification) {
		// Cette m�thode scan le fichier membre.csv, ajoute toutes les entr�es dans un String en prenant en compte les changements voulus
		// Elle retourne un string avec les changements. Il ne reste ensuite qu'� �crire remplacer le contenu du fichier original par le
		// nouveau contenu du string que cette m�thode retourne.
		try {
			ouvrirFichier("membre.csv");
		}
		catch (Exception e) {
			System.out.println("Impossible d'ouvrir/de lire le fichier membre.csv");
			return null;
		}

		String temp= "";
		String ident;
		boolean found =false;
		int identInt=0;
		while(scanner.hasNext()){
			ident = scanner.next();
			try {identInt =Integer.parseInt(ident);}
			catch (Exception e) {}
			if (ident == "id" || identInt != membreId) {
				temp+= ident;
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+=",";
				temp+=scanner.next();
				temp+='\n';
			}
			else {
				if (champ.equals("effacer")) { // S'il faut effacer une entr�e, on fait juste 'skipper' les champs du Id entr� en argument
					scanner.next();
					scanner.next();
					scanner.next();
				}
				else { // Pour une modification
					temp+= ident;
					temp+=",";
					if (champ.equals("nom")) { //Si on veut modifier len om
						temp+=modification;
						scanner.next();
					}
					else {temp+=scanner.next();}//Sinon on garde le m�me nom
					temp+=",";
					if (champ.equals("courriel")) { // Si on veut modifier le courriel
						temp+=modification;
						scanner.next();
					}
					else {temp+=scanner.next();}//Sinon m�me courriel
					temp+=",";
					if (champ.equals("paye")) { //Si on veut modifier le statut de paiement
						temp+=modification;
						scanner.next();
					}
					else {temp+=scanner.next();}//Sinon m�me statut
					temp+='\n';
				}
				found=true;
			}
		}
		scanner.close();
		if (found) {
			return temp;
		}
		else {
			System.out.println("Erreur: Pas de membre avec ce code dans le systeme");
			return null;
		}
	}
	public static String listeInscriptions(int profId) {
		try {
			ouvrirFichier("seance.csv");
		}
		catch (Exception e) {
			System.out.println("Impossible d'ouvrir/de lire le fichier seance.csv");
			return null;
		}

		String inscriptions= "Professionnel: " +profId + '\n';
		String date="";
		String idMembre="";
		String idService="";
		int idProfes=0;
		String idSeance;
		while(scanner.hasNext()){
			idSeance = scanner.next();

			if (idSeance.equals("Id seance")) {
				scanner.next();
				scanner.next();
				scanner.next();
				scanner.next();
				scanner.next();
				scanner.next();
			}
			else {
				scanner.next();//date creation skip
				date =scanner.next();//date service 
				idMembre=scanner.next();
				idProfes = Integer.parseInt(scanner.next());
				idService = scanner.next();
				scanner.next();//skip commentaires
				if (idProfes == profId) {
					inscriptions+= "Seance: "+idSeance+ " Date: "+date+" Service: "+idService+ " Membre: "+idMembre +'\n';
				}
				
			}
			
		}
		scanner.close();

		if (inscriptions.equals(Integer.toString(profId))) {
			System.out.println("AucuneInscription");
			return null;
		}
		return inscriptions;
	}
}
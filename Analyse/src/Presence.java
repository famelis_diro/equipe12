/*
 * presence est une seance dont le membre a
 * confirme sa presence
 */

import java.time.LocalDateTime;

public class Presence {
	int id;
	LocalDateTime dateHeureCreation;
	int profId;
	int memId;
	int serviceId;
	String commentaires = "";
	
	public Presence(int id, int profId, int memId, int serviceId, String commentaires){
		this.id = id;
		this.dateHeureCreation = java.time.LocalDateTime.now();
		this.profId = profId;
		this.memId = memId;
		this.serviceId = serviceId;
		this.commentaires = commentaires;
	}
	public Presence(int id, LocalDateTime dateCreer, int profId, int memId, int serviceId, String commentaires){
		this.id = id;
		this.dateHeureCreation = dateCreer;
		this.profId = profId;
		this.memId = memId;
		this.serviceId = serviceId;
		this.commentaires = commentaires;
	}
}

/*
 * Cette classe s'occupe de faire le rapport comptable
 * Veuillez regarder les commentaires
 * Du code a ete commenter, mais est tout de meme fonctionnel
 * 
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class ServiceComptable {
	
	//Cette fonction prepare un String qui utiliser pour faire un fichier CSV
	public static String preparerRapport() {
		Map<Integer, Professionnel>dictionnaireProf = LireCSV.hashProfessionnel();
		Map<Integer, Service>dictionnaireService = LireCSV.hashService();
		StringBuilder sb = new StringBuilder();
		// Les deux montants ici sont utilses pour calculer les totaux pour tout #GYM
		double montantTotalDollars = 0;
		int montantTotalServices = 0;
		
		// Cette boucle regarde chaque professionnel
		for (Map.Entry<Integer,Professionnel> prof : dictionnaireProf.entrySet()) {
			// Ces variables servent a calculer les totaux pour chaque professionnel
			double montantDollarProf = 0;
			int montantServiceProf = 0;
			
			Professionnel p = prof.getValue();
			sb.append("Nom Professionnel");
			sb.append(',');
			sb.append("Id");
			sb.append("\r\n");
			sb.append(p.nom);
			sb.append(',');
			sb.append(p.id);
			sb.append("\r\n");
			sb.append("\r\n");
			
			// Cette boucle regarde chaque service donnee par un professionel
			for (Map.Entry<Integer,Service> service : dictionnaireService.entrySet()) {
				Service s = service.getValue();
				int montantSeances = 0;
				if(s.profId == p.id) {
					montantSeances = calculerNombreSeances(s);
					montantDollarProf += montantSeances * s.frais;
					montantServiceProf += 1;
					sb.append("Nom Service");
					sb.append(',');
					sb.append("Id");
					sb.append(',');
					sb.append("Nombre seance");
					sb.append(',');
					sb.append("Montant total pour le service");
					sb.append("\r\n");
					sb.append(s.nom);
					sb.append(',');
					sb.append(s.id);
					sb.append(',');
					sb.append(montantSeances);
					sb.append(',');
					sb.append(montantSeances * s.frais);
					sb.append("\r\n");
					sb.append("\r\n");
				}
			}
			montantTotalDollars += montantDollarProf;
			montantTotalServices += montantServiceProf;
			sb.append("Montant Total:");
			sb.append(montantDollarProf);
			sb.append(',');
			sb.append("Nombre Services:");
			sb.append(montantServiceProf);
			sb.append("\r\n");
			sb.append("\r\n");
			sb.append("\r\n");
		}
		sb.append("Montant total des services:");
		sb.append(montantTotalDollars);
		sb.append(",");
		sb.append("Nombre total de services");
		sb.append(montantTotalServices);
		
		return sb.toString();
	}
	// Cette fonction retourne le nombre d'inscriptions pour une activite
	private static int calculerNombreSeances(Service service) {
		int montant = 0;
		Map<Integer, Seance>dictionnaireSeance = LireCSV.hashSeance();
		for (Map.Entry<Integer, Seance> seance : dictionnaireSeance.entrySet()) {
			if(seance.getValue().serviceId == service.id) {
				montant++;
			}
		}
		return montant;
	}
	// Cette fonction cree le rapport
	public static void creerRapportComptable(String s) throws FileNotFoundException{
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File("RapportComptable.csv"),false));
		pw.write(s);
		pw.close();
	}
	
	public static void envoyerCourriel(String email) {
		// Le code pris pour cette methode est disponible a 
		// https://stackoverflow.com/questions/16117365/sending-mail-attachment-using-java
		//*****************************************************************************************
		// NB pour avoir acces a ce code, il faut telecharge javax.mail et l'ajouter au build path du projet
		// Apres avoir decommenter, allez decommenter la partie correspondante dans la classe GUI
		// Vous pourez ensuite envoyer le rapport a un email via la console
		
		/*
		String username = "devoir2ift2255@gmail.com";
		String password = "IFT2255D2";

		Properties props = new Properties();
	    props.put("mail.smtp.auth", true);
	    props.put("mail.smtp.starttls.enable", true);
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.port", "587");
		
	    Session session = Session.getInstance(props,
	            new javax.mail.Authenticator() {
	                protected PasswordAuthentication getPasswordAuthentication() {
	                    return new PasswordAuthentication(username, password);
	                }
	            });
	    try {

	        Message message = new MimeMessage(session);
	        message.setFrom(new InternetAddress("devoir2ift2255@gmail.com"));
	        message.setRecipients(Message.RecipientType.TO,
	                InternetAddress.parse(email));
	        message.setSubject("Testing Subject");
	        message.setText("PFA");

	        MimeBodyPart messageBodyPart = new MimeBodyPart();

	        Multipart multipart = new MimeMultipart();

	        messageBodyPart = new MimeBodyPart();
	        String file = "C:\\Projects\\equipe12\\Devoir2\\RapportComptable.csv";
	        String fileName = "RapportComptable.csv";
	        DataSource source = new FileDataSource(file);
	        messageBodyPart.setDataHandler(new DataHandler(source));
	        messageBodyPart.setFileName(fileName);
	        multipart.addBodyPart(messageBodyPart);

	        message.setContent(multipart);

	        System.out.println("Sending");

	        Transport.send(message);

	        System.out.println("Done");

	    } catch (MessagingException e) {
	        e.printStackTrace();
	    }
	    */
	  }
		
	
	
	public static void main(String[] args) throws Exception{
		/*
		 * Ici est notre solution pour envoyer la procedure comptable
		 *  les vendredis a la bonne heure.  j'ai commenter le code, car
		 * ce n'est pas vraiment mesurable et il manque la partie envoyerEmail
		 * 
	    TimerTask task = new TimerTask() {
	      @Override
	      public void run() {
				try {
					ServiceComptable.creerRapportComptable(ServiceComptable.preparerRapport());
				} catch (FileNotFoundException e) {
					System.out.println(e);
				}
				ServiceComptable.envoyerCourriel("service_comptable@rnb.ca");
	      }
	    };
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date date = dateFormatter.parse("2018-11-16 23:59:59");

	    Timer timer = new Timer();

	    //604800000 ms dans une semaine
	    timer.schedule(task, date,604800000);
	}
    */
	}
}

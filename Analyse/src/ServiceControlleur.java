/*
 * fait les manipulations reliees auz services
 */
import java.time.*;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Map;


public class ServiceControlleur {
	EcrireCSV csv = new EcrireCSV();
	
	public void creerService(String name, 
			String dateDebServiceStr, 
			String dateFinServiceStr, 
			String heureServiceStr, 
			int[] joursSemaines, 
			int capaciteMaximale, 
			int noProfessionnel, 
			double frais,
			String commentaires) throws FileNotFoundException {
		GenerateurId faireId = new GenerateurId("service");
		
		Service nouveauService = new Service(faireId.id,
				name, 
				creerDate(dateDebServiceStr),
				creerDate(dateFinServiceStr),
				creerHeure(heureServiceStr),
				creerRecurrenceHebdomadaire(joursSemaines),
				capaciteMaximale,
				noProfessionnel,
				frais,
				commentaires);
		csv.ecrireNouveauService(nouveauService);	
		
	}
	// converti entree en type de date
	public static LocalDate creerDate(String dateStr){
		int annee = Integer.parseInt(dateStr.substring(6,10));
		int mois = Integer.parseInt(dateStr.substring(3,5));
		int jour = Integer.parseInt(dateStr.substring(0,2));
		LocalDate date = LocalDate.of(annee, mois, jour);
		return date;
	}
	//converti entree en type heure
	private static LocalTime creerHeure(String tempsStr) {
		int heure = Integer.parseInt(tempsStr.substring(0,2));
		int mins = Integer.parseInt(tempsStr.substring(3,5));
		LocalTime temps = LocalTime.of(heure,mins);
		return temps;
	}
	// Foramtte entree pour avoir tableau de string
	private String[] creerRecurrenceHebdomadaire(int[] jours) {
		int nombreJours = 0;
		for(int i = 0; i < jours.length; i++) {
			if(jours[i] == 1) {
				nombreJours++;
			}
		}
		String[] recurrenceHebdomadaire = new String[nombreJours];
		int jour = 0;
		for(int j = 0; j < jours.length; j++) {
			if(jours[j] == 1) {
				recurrenceHebdomadaire[jour] = DayOfWeek.of(j+1).getDisplayName(TextStyle.SHORT,new Locale("fr"));
				jour++;
			}
		}
		return recurrenceHebdomadaire;
	}
	
	//Le professionnel annule son service
	public void supprimerService() {
		//Suppression de toutes les paires Membre-service ayant le code du service annul� dans le fichier Inscription
		//Supression du service en question dans le fichier Service
	}
	
	//Le membre veut consulter le r�pertoire des services offerts	
	public void consulterRepServices() {
		String temp =LireCSV.lireRepertoireServices();
		System.out.println(temp);
		
	}
	
	
	//Le professionnel veut consulter les inscriptions � son service
	public void consulterInscription() {
		//Lecture du fichier CSV inscription
		//Affichage du fichier CSV inscription
	}
	
	public static void effacerService(int servId) {
		String temp = LireCSV.nouveauRepertoireServicesSansUneEntree(servId);
		if (temp==null) {
			System.out.println();
			return;
		}
		EcrireCSV.ecrireNouveauRepertoire(temp, "services.csv");
	}

		
		/*
		Service service;
		Map<Integer,Professionnel> dictionnaireProfessionnel = LireCSV.hashProfessionnel();
		Professionnel prof =dictionnaireProfessionnel.get(profId);
		String[] services = prof.services.split("-");
		Map<Integer,Service> dictionnaireService = LireCSV.hashService();
		Map<Integer,Seance> dictionnaireSeance = LireCSV.hashSeance();
		dictionnaireSeance.get
		for (int i=0;i<services.length;i++) {
			service = dictionnaireService.get(Integer.parseInt(services[i]));
			//for (int j=0; j<)
		}
		//tring services
		
		
		System.out.println();
		*/
		
	
}

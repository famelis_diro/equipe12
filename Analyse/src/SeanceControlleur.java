
/*
 * Ici on fait les mainpulations avec les seances
 * On gere aussi les presences 
 */

import java.io.FileNotFoundException;
import java.util.Map;

public class SeanceControlleur {
	EcrireCSV csv = new EcrireCSV();
	
	public void creerPresence(int memId, int serviceId, String commentaires) throws FileNotFoundException{
	
	Map<Integer,Service> dictionnaireService = LireCSV.hashService();
	// TODO ajouter try catch pas de cle trouver
	Service service = dictionnaireService.get(serviceId);
	GenerateurId faireId = new GenerateurId("presence");
	
	Presence presence = new Presence(faireId.id, service.profId, memId, serviceId, commentaires);
	csv.ecrireNouvellePresence(presence);
	}
	
	public double creerSeance (int memId, int serviceId, String dateStr, String commentaires) throws FileNotFoundException{
		// TODO ajouter try catch si pas de cle
		Map<Integer,Service> dictionnaireService = LireCSV.hashService();
		Service service = dictionnaireService.get(serviceId);
		GenerateurId faireId = new GenerateurId("seance");
		
		Seance seance = new Seance(faireId.id, ServiceControlleur.creerDate(dateStr), memId, service.profId, serviceId, commentaires);
		csv.ecrireNouvelleSeance(seance);
		return service.frais;
	}
	
	public void consulterInscriptionsSeances(int profId) {
		String temp = LireCSV.listeInscriptions(profId);
		System.out.println(temp);
	}
}

/*
 * Cette classe gere les manipulations avec les professionnels
 */

import java.io.FileNotFoundException;

public class ProfessionnelControlleur {
	
	EcrireCSV csv = new EcrireCSV();

	public void creeProfessionnel(String name, String email, String services) throws FileNotFoundException {
		
		GenerateurId faireId = new GenerateurId("professionnel");
		
		Professionnel nouveauProfessionnel = new Professionnel(faireId.id, name,email,services);
		csv.ecrireNouveauProfessionnel(nouveauProfessionnel);
	}
}


/* Ceci est la classe scanner
 * Executer le jar active la console
 * On tappe les commandes suivi par enter
 * Le case gere chaque commande
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

enum Status 
{ 
    Valide, Nonvalide, Suspendu
}

public class GUI {
	static Scanner sc;
	static MembreControlleur mc;
	static ServiceControlleur sct;
	static ProfessionnelControlleur pc;
	static SeanceControlleur sec;
	
	public static void main(String args[]) throws IOException {
		sc = new Scanner(System.in);
		mc = new MembreControlleur();
		sct = new ServiceControlleur();
		pc = new ProfessionnelControlleur();
		sec = new SeanceControlleur();
		boolean rester = true;
		
		System.out.println("Bienvenue! pour de l'aide, entrez 'aide' suivi de enter.");
		while(rester) {
			String command  = sc.nextLine();
	    	switch(command) {
			    case "aide":
			    	aide();
			    	break;
			    case "v":
			    	validerMembre();
			    	break;    
			    case "m":
			    	creerMembre();
			    	break;
			    case "s":
			    	creerService();
			    	break;
			    case "x":
			    	modifierMembreGUI();
			    	break;
			    case "t":
			    	consulterRepetoireServices();
			    	break;
			    case "n":
			    	consulterInscriptionsSeances();
			    	break;
			    case "i":
			    	creerSeance();
			    	break;
			    case "o":
			    	creerProfessionnel();
			    	break;
			    case "p":
			    	paiementMembre();
			    	break;
			    case "d":
			    	effacerMembreGUI();
			    	break;
			    case "e":
			    	effacerServiceGUI();
			    	break;
			    case "r":
			    	confirmerPresence();
			    	break;
			    case "c":
			    	envoyerRapportComptable();
			    	break;
			    case "q":
			    	System.out.println("Fin de l'execution");
			    	rester = false;
			    	sc.close();
			    default:
			    	continue;
			    }
	    	System.out.println("Entrez une autre commande:");
		    }
	    sc.close();
	}
	private static void modifierMembreGUI() {
		String modification = "";
		System.out.println("Entrez le code du membre que vous voulez modifier:");
		int membreId = sc.nextInt();
		System.out.println("Est-ce que vous voulez changer le nom ? 1:oui 0:non");
		if (sc.nextInt() == 1) {
			System.out.println("Quel est le nouveau nom ?");
			modification = sc.next();
			MembreControlleur.modifierMembre(membreId, "nom", modification);
		}
		System.out.println("Est-ce que vous voulez changer le courriel ? 1:oui 0:non");
		if (sc.nextInt() == 1) {
			System.out.println("Quel est le nouveau courriel ?");
			modification = sc.next();
			MembreControlleur.modifierMembre(membreId, "courriel", modification);
		}
		System.out.println("Retour au menu.");
	}
	private static void consulterInscriptionsSeances() {
		System.out.println("Entrez le code du professionnel: ");
		int profId = sc.nextInt();
		System.out.println("Voici les inscriptions � toutes les s�ances donn�es par le professionnel");
		sec.consulterInscriptionsSeances(profId);
		
	}
	private static void consulterRepetoireServices() {
		System.out.println("Voici les services offerts actuellement par le #GYM");
		sct.consulterRepServices();
		
	}
	private static void envoyerRapportComptable() throws FileNotFoundException {
		// System.out.println("Voulez vous envoyer le rapport par email?");
		//System.out.println("le rapport sauvera sur votre disque peu importe la reponse");
		ServiceComptable.creerRapportComptable(ServiceComptable.preparerRapport());
		System.out.println("Rapport Genere");
		// Si vous avez activer la fonction envoyerCourriel dans ServiceComptable, decommentez le code
		/*
		System.out.println("1 = oui 0 = non");
		if(sc.nextInt() == 1) {
			System.out.println("Entrez le email a qui vous voulez envoyer le rapport comptable:");
			String courriel = sc.next();
			ServiceComptable.creerRapportComptable(ServiceComptable.preparerRapport());
			ServiceComptable.envoyerCourriel(courriel);
		} else {
			ServiceComptable.creerRapportComptable(ServiceComptable.preparerRapport());
		}
		*/
	}
	private static void creerSeance() throws FileNotFoundException{
		System.out.println("Entrez le code du membre qui veut s'inscrire a un service:");
		// TODO: valider numero membre
		int memId = sc.nextInt();
		System.out.println("Inscrivez le code de service associe a la seance");
		int serviceId = sc.nextInt();
		System.out.println("Entrez la date du service (JJ-MM-AAAA)");
		String dateSeance = sc.next();
		System.out.println("Ajoutez des commentaires ici si necessaire");
		String commentaires = sc.nextLine();
		//TODO Valider longueur des commentaire (<100)
		double frais = sec.creerSeance(memId, serviceId, dateSeance, commentaires);
		System.out.println(frais);
	}
	
	private static void confirmerPresence() throws FileNotFoundException {
		System.out.println("Entrez le code du membre qui veut confirmer sa presence:");
		int memId = sc.nextInt();
		System.out.println("Ouvrez le repertoire de services nomme services.csv afin de trouver le bon code de service.");
		System.out.println("Inscrivez le code de service ici:");
		int serviceId = sc.nextInt();
		System.out.println("Ajoutez des commentaires ici si necessaire");
		String commentaires = sc.next();
		sec.creerPresence(memId, serviceId, commentaires);
	}

	private static void creerProfessionnel() throws FileNotFoundException {
		System.out.println("Entrer le nom du nouveau professionnel:");
		String name = sc.next();
		System.out.println("Entrer l'addresse courriel du nouveau professionnel:");
		String email = sc.next();
		System.out.println("Entrez le code de chaque service propos� par le professionnel de la mani�re suivante : 'service1-service2'.");
		System.out.println("Si aucun service inscrit pour l'instant, seulement entrer la commande 'enter'");
		String services = sc.next();
		pc.creeProfessionnel(name, email, services);
		
	}

	private static void effacerServiceGUI() {
		int servId;
		System.out.println("Entrer le code du membre que vous voulez effacer, suivi de 'enter':");
		String input = sc.next();
		try {
			servId =Integer.parseInt(input);
			ServiceControlleur.effacerService(servId);
		}
		catch (Exception e) {
			System.out.println("Num�ro non valide.");
			return;
		}
	
	}
	private static void effacerMembreGUI() throws IOException {
		int	memId;
		System.out.println("Entrer le code du membre que vous voulez effacer, suivi de 'enter':");
		String input = sc.next();
		try {
			memId =Integer.parseInt(input);
			MembreControlleur.modifierMembre(memId, "effacer", null);
		}
		catch (Exception e) {
			System.out.println("Num�ro non valide.");
			return;
		}
	}
	public static void validerMembre() {
    	System.out.println("Entrer le numero de membre:");
    	int memId = sc.nextInt();
    	Status status = mc.validerMembre(memId);
		if(status == Status.Valide){
    		System.out.println("Valide");
		} else {
			if(status == Status.Suspendu){
			System.out.println("Membre suspendu");
			} else {
				System.out.println("Numero invalide");
			}
		} 
	}
	
	private static void paiementMembre() {
		System.out.println("Entrez le num�ro de membre:");
		int memId = sc.nextInt();
		Status status = mc.validerMembre(memId);
		if(status == Status.Valide){
    		System.out.println("Paiement non-n�cessaire");
		} 
		else {
			if(status == Status.Suspendu){
				System.out.println("Veuillez effecuter le paiement");
				System.out.println("Si le paiement a fonctionn�, pesez '1', puis'Enter'.");
				System.out.println("Si le paiement a �chou�, pesez '0', puis'Enter'.");
				int choiceId = sc.nextInt();
				if (choiceId == 0) {
					return;
				}
				else if (choiceId ==1) {
					MembreControlleur.modifierMembre(memId, "paye", "oui");
					System.out.println("");
				}
				else {
					System.out.println("Mauvais choix");
				}
			
			} 
			else {
				System.out.println("Numero invalide");
			}
		} 
	}
	
	public static void creerMembre() throws FileNotFoundException {
		System.out.println("Entrer le nom du nouveau membre:");
		String name = sc.nextLine();
		System.out.println("Entrer l'addresse courriel du nouveau membre:");
		String email = sc.nextLine();
		mc.creeMembre(name, email);
	}
	public static void creerService() throws FileNotFoundException {
		int[] joursSemaine = new int[7];
		
		//TODO parse les dates et heures
		System.out.println("Entrer le nom du service:");
		String name = sc.next();
		
		System.out.println("Indiquez la date de debut du service (JJ-MM-AAAA)");
		String dateDebServiceStr = sc.next();
		//LocalDate dateDebService = LocalDate.parse(dateDebServiceStr);
		
		//TODO verifier format date avec une fonction
		System.out.println("Indiquez la date de fin du service (JJ-MM-AAAA)");
		//TODO verifier format date avec une fonction
		String dateFinServiceStr = sc.next();
		//LocalDate dateFinService = LocalDate.parse(dateFinServiceStr);
		
		System.out.println("Indiquez l'heure du service (HH:MM)");
		String heureServiceStr = sc.next();
		//LocalTime heureService = LocalTime.parse(heureServiceStr);
		//fonction pour valider l'heure
		
		
		System.out.println("Indiquez pour chaque jour si le service aura lieu (1=oui, 0=non) ");
		
		//TODO refacter peut etre et ajouter validation pour 0 ou 1 comme input
		System.out.println("Lundi:");
		joursSemaine[0] = sc.nextInt();
		System.out.println("Mardi:");
		joursSemaine[1] = sc.nextInt();
		System.out.println("Mercredi:");
		joursSemaine[2] = sc.nextInt();
		System.out.println("Jeudi:");
		joursSemaine[3] = sc.nextInt();
		System.out.println("Vendredi:");
		joursSemaine[4] = sc.nextInt();
		System.out.println("Samedi:");
		joursSemaine[5] = sc.nextInt();
		System.out.println("Dimanche:");
		joursSemaine[6] = sc.nextInt();

		
		System.out.println("Indiquez la capacite maximal du service");
		int capaciteMaximale = sc.nextInt();
		System.out.println("Indiquez le code de professionnel du service");
		int noProfessionnel = sc.nextInt();
		System.out.println("Indiquez les frais pour ce service");
		double frais = Double.parseDouble(sc.next());
		System.out.println("Indiquez des commentaires (facultatif)");
		String commentaires = sc.nextLine();

		sct.creerService(name, 
				dateDebServiceStr, 
				dateFinServiceStr, 
				heureServiceStr, 
				joursSemaine,  
				capaciteMaximale, 
				noProfessionnel, 
				frais,
				commentaires);
		
	}
	private static void aide() {
		System.out.println("Aide, voici les commandes:");
		System.out.println("'r' permet de valider une inscription d'un membre");
		System.out.println("'v' permet de valider un numero de membre");
		System.out.println("'m' permet de creer un nouvau membre");
		System.out.println("'i' permet d'inscrire un membre a un service");
		System.out.println("'d' permet de supprimer un membre existant");
		System.out.println("'i' permet de creer une seance");
		System.out.println("'x' permet de modifier un membre existant");
		System.out.println("'e' permet de supprimer un service existant");
		System.out.println("'q' permet de quitter le programme");
		System.out.println("'n' permet de consulter les inscriptions de toutes les s�ances d'un professionnel");
		System.out.println("'t' permet de consulter le r�pertoire des services disponibles");
		System.out.println("'o' permet de creer un nouveau professionnel");
		System.out.println("'p' permet de faire payer les frais mensuels � un membre");
		System.out.println("'s' permet de creer un service");
		System.out.println("'c' permet d'envoyer et/ou generer le rapport comptable a un courriel");
		System.out.println("Tapez enter apres chaque commande");
	}
}

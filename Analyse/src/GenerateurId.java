/*
 * Cette classe genere un id unique 
 * selon quel type d'item nous voulons. 
 * Afin d'avoir un nombre de chiffres appropries
 * nous n'acceptons pas 0 comme chiffre prefix
 * Nous faisons cela de facon aleatoire pour des raisons
 * de securite  
 */

import java.util.Map;
import java.util.Random;

public class GenerateurId {
	int id;

	public GenerateurId(String s) {
		switch(s) {
		case "membre":
			this.id = creerIdMembre();
			break;
		case "service":
			this.id = creerIdService();
			break;
		case "presence":
			this.id = creerIdPresence();
			break;
		case "professionnel":
			this.id = creerIdProfessionnel();
			break;
		case "seance":
			this.id = creerIdSeance();
			
		}
	}
	private int creerIdMembre() {
		Boolean estUnique = false;
		int numero = 0;
		while(!estUnique) {
			Random ran = new Random();
			numero = ran.nextInt(999999999) + 100000000;
			Map<Integer,Membre> map = LireCSV.hashMembre();
			if(map.get(numero) == null) {
				estUnique = true;
			}
		}
		System.out.println("Num�ro du nouveau membre: " + numero);
		return numero;
	}
	private int creerIdService() {
		Boolean estUnique = false;
		int numero = 0;
		while(!estUnique) {
			Random ran = new Random();
			numero = ran.nextInt(9999999) + 1000000;
			Map<Integer,Service> map = LireCSV.hashService();
			if(map.get(numero) == null) {
				estUnique = true;
			}
		}
		System.out.println("Num�ro du nouveau service: " + numero);
		return numero;
	}
	private int creerIdProfessionnel() {
		Boolean estUnique = false;
		int numero = 0;
		while(!estUnique) {
			Random ran = new Random();
			numero = ran.nextInt(999999999) + 100000000;
			Map<Integer,Professionnel> map = LireCSV.hashProfessionnel();
			if(map.get(numero) == null) {
				estUnique = true;
			}
		}
		System.out.println("Num�ro du nouveau professionnel: " + numero);
		return numero;
	}
	private int creerIdPresence() {
		Boolean estUnique = false;
		int numero = 0;
		while(!estUnique) {
			Random ran = new Random();
			numero = ran.nextInt(999999) + 100000;
			Map<Integer,Presence> map = LireCSV.hashPresence();
			if(map.get(numero) == null) {
				estUnique = true;
			}
		}
		return numero;
	}
	private int creerIdSeance() {
		Boolean estUnique = false;
		int numero = 0;
		while(!estUnique) {
			Random ran = new Random();
			numero = ran.nextInt(999999) + 100000;
			Map<Integer,Seance> map = LireCSV.hashSeance();
			if(map.get(numero) == null) {
				estUnique = true;
			}
		}
		return numero;
	}
}

/*
 * Cette classe gere les manipulations reliees aux membres
 */

import java.io.FileNotFoundException;
import java.util.Map;

public class MembreControlleur {
	
	EcrireCSV csv = new EcrireCSV();
	
	// Va donner acces au membre, retourne son status  
	public Status  validerMembre(int memId) {
		Map<Integer,Membre> dictionnaireMembre = LireCSV.hashMembre();
		Membre membre = dictionnaireMembre.get(memId);
		if(membre == null) {
			return Status.Nonvalide;
		}
		if(membre.paye.equals("oui")||membre.paye.equals("oui\r")) {
			return Status.Valide;
		}
			return Status.Suspendu;		
	}
	
	public void creeMembre(String name, String email) throws FileNotFoundException {
		
		GenerateurId faireId = new GenerateurId("membre");  
		Membre nouveauMembre = new Membre(faireId.id,name,email,"oui");
		csv.ecrireNouveauMembre(nouveauMembre);
	}
	
	public static int creeId() {
			return LireCSV.hashMembre().size() + 1;
	}

	public static void modifierMembre(int membreId, String champ, String modification) {
		String temp = LireCSV.nouveauRepertoireMembre(membreId, champ, modification);
		if (temp==null) {
			return;
		}
		EcrireCSV.ecrireNouveauRepertoire(temp, "membre.csv");
	}

}

/* 
 * Une seance est comme une inscription a une activite
 */

import java.time.*;

public class Seance {
	int id;
	LocalDateTime dateHeureCreation;
	LocalDate dateService;
	int memId;
	int profId;
	int serviceId;
	String commentaires = "";
	
	// Ce constructeur sert a creer des nouvelles seances dans le controlleur
	public Seance(int id, LocalDate dateService, int memId, int profId, int serviceId, String commentaires) {
		this.id = id;
		dateHeureCreation = LocalDateTime.now();
		this.dateService = dateService;
		this.memId = memId;
		this.profId = profId;
		this.serviceId = serviceId;
		this.commentaires = commentaires;
		
	}
	// Ce constructeur est utiliser pour le faire le dictionnaire, puisque dateHeureCreation existe deja
	public Seance(int id, LocalDateTime dateHeureCreation, LocalDate dateService, int memId,  int profId, int serviceId, String commentaires) {
		this.id = id;
		this.dateHeureCreation = dateHeureCreation;
		this.dateService = dateService;
		this.memId = memId;
		this.profId = profId;
		this.serviceId = serviceId;
		this.commentaires = commentaires;
	}
}

//test

/* Cette classe ecrit un fichier csv 
 * les colonnes devrait etre specifiee dans une autre classe
 * Cette classe sert a ecrire dans le csv
 * 
 */

// import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.util.Arrays;

public class EcrireCSV {
	public void ecrireNouveauMembre(Membre m) throws FileNotFoundException{
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File("membre.csv"),true));

		StringBuilder sb = new StringBuilder();
        sb.append(m.id);
        sb.append(',');
        sb.append(m.nom);
        sb.append(",");
        sb.append(m.email);
        sb.append(",");
        sb.append(m.paye);
        sb.append("\r\n");
        pw.write(sb.toString());
        pw.close();
        System.out.println("Nouveau membre cree!");
	}
	
	public static void ecrireNouveauRepertoire(String nouveauContenu, String fichierCSV) {
		// cette m�thode Modifie un fichier sp�cifi� en argument.
		// Prend le contenu de la string en argument et l'�crit sur le fichier sp�cifi�.
		try {
			Files.deleteIfExists(Paths.get(fichierCSV));
			PrintWriter pw = new PrintWriter(new FileOutputStream(new File(fichierCSV),true));
			pw.write(nouveauContenu);
			pw.close();
		}
		catch (IOException e) {
			System.out.println("Erreur avec fichier "+fichierCSV+" : Le r�pertoire n'a pas �t� modifi�");
			return;
		}
		System.out.println("L'item a �t� modifi�/supprim� de la base de donn�es");
	}
	
	public void ecrireNouveauService(Service s) throws FileNotFoundException{
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File("services.csv"),true));

		StringBuilder sb = new StringBuilder();
		sb.append(s.id);
        sb.append(",");
        sb.append(s.nom);
        sb.append(',');
        sb.append(s.dateHeureActuelles);
        sb.append(",");
        sb.append(s.dateDebService);
        sb.append(",");
        sb.append(s.dateFinService);
        sb.append(",");
        sb.append(s.heureService);
        sb.append(",");
        sb.append(formatRecurrence(s.recurrenceService));
        sb.append(",");
        sb.append(s.capaciteMaximale);
        sb.append(",");
        sb.append(s.profId);
        sb.append(",");
        sb.append(s.frais);
        sb.append(",");
        sb.append(s.commentaires);
        sb.append("\r\n");
        pw.write(sb.toString());
        pw.close();
        System.out.println("Nouveau service cr��!");
	}
	
	private String formatRecurrence(String[] recurrence) {
		StringBuilder format = new StringBuilder(Arrays.toString(recurrence));
		for(int i = 0; i < format.length(); i++) {
			if(format.substring(i,i+1).equals(",")){
				format.setCharAt(i, ' ');
			}
		}
		return format.toString();
	}
	
	public static void main(String[] args) {
		
	}

	public void ecrireNouveauProfessionnel(Professionnel p)  throws FileNotFoundException{
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File("professionnel.csv"),true));

		StringBuilder sb = new StringBuilder();
        sb.append(p.id);
        sb.append(',');
        sb.append(p.nom);
        sb.append(",");
        sb.append(p.email);
        sb.append(",");
        sb.append(p.services);
        sb.append("\r\n");
        pw.write(sb.toString());
        pw.close();
        System.out.println("Nouveau professionnel cree!");
	}
	public void ecrireNouvellePresence(Presence p)  throws FileNotFoundException{
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File("presence.csv"),true));

		StringBuilder sb = new StringBuilder();
        sb.append(p.id);
        sb.append(',');
        sb.append(p.dateHeureCreation);
        sb.append(",");
        sb.append(p.profId);
        sb.append(",");
        sb.append(p.memId);
        sb.append(",");
        sb.append(p.serviceId);
        sb.append(",");
        sb.append(p.commentaires);
        sb.append("\r\n");
        pw.write(sb.toString());
        pw.close();
        System.out.println("Presence confirme!");
	}

	public void ecrireNouvelleSeance(Seance s) throws FileNotFoundException{
		PrintWriter pw = new PrintWriter(new FileOutputStream(new File("seance.csv"),true));

		StringBuilder sb = new StringBuilder();
        sb.append(s.id);
        sb.append(',');
        sb.append(s.dateHeureCreation);
        sb.append(",");
        sb.append(s.dateService);
        sb.append(",");
        sb.append(s.memId);
        sb.append(",");
        sb.append(s.profId);
        sb.append(",");
        sb.append(s.serviceId);
        sb.append(",");
        sb.append(s.commentaires);
        
        sb.append("\r\n");
        pw.write(sb.toString());
        pw.close();
        System.out.println("Prix a payer:");
	}
}
